/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --disable-multilib --enable-languages=c --target=arm-linux-gnueabi --prefix=/home/frap129/tc/gnu/arm-linux-gnueabi --enable-graphite=yes --enable-gold -disable-werror";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "tls", "gnu" } };
